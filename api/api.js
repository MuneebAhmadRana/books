const BASE_URL = "http://localhost:3000";

const extractJson = (response) => {
  return response.json();
};

export const fetchBooks = () => {
  return fetch(`${BASE_URL}/books`).then(extractJson).catch(console.log);
};

export const addBook = (book) => {
  return fetch(`${BASE_URL}/books`, {
    method: "POST", // or 'PUT'
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(book),
  })
    .then(extractJson)
    .catch(() => {
      return false;
    });
};
