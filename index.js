import { Book } from "./book.js";
import { fetchBooks, addBook } from "./api/api.js";
class App {
  // DOM elements
  elBookCollectionDiv = document.getElementById("book-collection");
  elSearchBooksDiv = document.getElementById("search-books");
  elAddBooksDiv = document.getElementById("add-books");
  elBookList = document.getElementById("book-list");
  /* elSubmitform = document.getElementById("submit-form"); */
  elAddBookButton = document.getElementById("add-book-button");
  elFormTitle = document.getElementById("form-book-title");
  elFormAuthor = document.getElementById("form-author");
  elFormCover = document.getElementById("form-cover-link");
  elSearchBarInput = document.getElementById("search-books-input");
  
  books = [];
  searching = false;
  searchArray = [];

  constructor() {}

  showBooks() {
    if (this.searching) {
      this.renderBooks(this.searchArray);
    } else {
      this.renderBooks(this.books);
    }
  }
  render() {
    this.showBooks();
  }

  renderBooks(booksToRender) {
    this.elBookList.innerHTML = "";
    booksToRender = booksToRender.sort((a, b) => {
      return a.title[0].toLowerCase() > b.title[0].toLowerCase();
    });
    
    booksToRender.forEach((book) => {
      const template = book.createTemplate();
      this.elBookList.innerHTML += template;
    });
  }

  async init() {
    try {
      this.books = await fetchBooks();
      this.books = this.books.map((book) => {
        return new Book(book.title, book.author, book.cover, book.read);
      });
      this.render();
    } catch (error) {
      console.log(error);
    }

    this.elAddBookButton.addEventListener('click', this.onSubmitBook.bind(this));
    this.elSearchBarInput.addEventListener('change', this.onBookSearch.bind(this));
  }

  async onSubmitBook() {
    const newBook = new Book(this.elFormTitle.value, this.elFormAuthor.value, this.elFormCover.value);
    this.elFormTitle.value = '';
    this.elFormAuthor.value = '';
    this.elFormCover.value = '';

    const responseBook = await addBook(newBook);
    if (!responseBook) {
      alert("Something went wrong!")
      return;
    }

    this.books.push(new Book(...Object.values(responseBook)));
    this.render();
  }

  onBookSearch() {
    console.log(this.elSearchBarInput.value)
    if (!this.elSearchBarInput.value) {
      this.searching = false;
      this.render();
      return;
    }
    this.searching = true;
    this.searchArray = this.books.filter(book => {
      if (book.title.includes(this.elSearchBarInput.value)) {
        return true;
      }
    })

    this.render();
  }
}

new App().init();
