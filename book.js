export class Book {
  constructor(title, author, cover, read = false) {
    this.title = title;
    this.author = author;
    this.cover = cover;
    this.read = read;
  }

  createTemplate() {
    return `
    <li>
      <div>
        <img src="${this.cover}"/>
      </div>
      <div>
        <label>${this.title}</label>
        <label>${this.author}</label>
        <button type="button">${this.read}</button>
      </div>
    </li>`;
  }
}
